package ua.transgas.ui;

import javax.faces.bean.ManagedBean;

import ua.transgas.dao.CatProtDaoException;
import ua.transgas.dao.PipelineDao;
import ua.transgas.domain.Pipeline;

@ManagedBean(name="createPipeline")
public class CreatePipeline {
	String name;
	int diameter;
	
	public CreatePipeline() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDiameter() {
		return diameter;
	}

	public void setDiameter(int diameter) {
		this.diameter = diameter;
	}
	
	public String create(){
		
		PipelineDao pipelineDao = new PipelineDao();
		Pipeline pipeline = new Pipeline();
		
		pipeline.setName(name);
		pipeline.setDiameter(diameter);
		
		try {
			pipelineDao.save(pipeline);
		} catch (CatProtDaoException e) {
			e.printStackTrace();
		}
		
		return "";
	}

}
