package ua.transgas.dao;

public class CatProtDaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public CatProtDaoException() {
	}

	public CatProtDaoException(String message) {
		super(message);
	}

	public CatProtDaoException(Throwable cause) {
		super(cause);
	}

	public CatProtDaoException(String message, Throwable cause) {
		super(message, cause);
	}

}
