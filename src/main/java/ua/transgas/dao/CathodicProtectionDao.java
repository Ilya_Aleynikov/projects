package ua.transgas.dao;

import ua.transgas.domain.CathodicProtection;

public class CathodicProtectionDao extends GenericDao<CathodicProtection> {

	public CathodicProtectionDao(Class<CathodicProtection> classT) {
		super(classT);
	}

}
