package ua.transgas.dao;

import ua.transgas.domain.Pipeline;

public class PipelineDao extends GenericDao<Pipeline> {

	public PipelineDao() {
		super(Pipeline.class);
	}

}
