/**
 * 
 */
package ua.transgas.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
//import org.hibernate.criterion.Projections;

public class GenericDao<T>{

	private Class<T> classT;
	private static final Logger LOG = LogManager.getLogger(GenericDao.class);

	public GenericDao(Class<T> classT) {
		this.classT = classT;
	}
	
    public void save(T object) throws CatProtDaoException {
    	LOG.info("Entered to PSQLGenericDao.save with object" + object);
		Session session = null;
        try {
            session = getSession();
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
        } catch (HibernateException e) {
        	LOG.error("Something went wrong with save transaction " + object, e);
        	throw new CatProtDaoException("Something went wrong with save transaction", e);
        } finally {
            closeSession(session);
        } 
    }

    public T retrieve(long id) throws CatProtDaoException {
    	LOG.info("Entered to PSQLGenericDao.retrieve with id" + id);
		Session session = null;
        T object = null;
        try {
            session = getSession();
            object = (T) session.get(classT, id);
        } catch (Exception e) {
        	LOG.error("Something went wrong with retrieve transaction " + id, e);
        	throw new CatProtDaoException("Something went wrong with retrieve transaction", e);
        } finally {
            closeSession(session);
        }
        return object;
    }

    public void update(T object) throws CatProtDaoException {
    	LOG.info("Entered to PSQLGenericDao.update with object" + object);
		Session session = null;
        try {
            session = getSession();
            session.beginTransaction();
            session.update(object);
            session.getTransaction().commit();            
        } catch (Exception e) {
        	LOG.error("Something went wrong with update transaction " + object, e);
        	throw new CatProtDaoException("Something went wrong with update transaction", e);
        } finally {
            closeSession(session);
        }
    }

    public void delete(T object) throws CatProtDaoException {
    	LOG.info("Entered to PSQLGenericDao.delete with object" + object);
		Session session = null;
        try {
            session = getSession();
            session.beginTransaction();
            session.delete(object);
            session.getTransaction().commit();            
        } catch (Exception e) {
        	LOG.error("Something went wrong with delete transaction " + object, e);
        	throw new CatProtDaoException("Something went wrong with delete transaction", e);
        } finally {
            closeSession(session);
        }
    }

	public List<T> retrieveAll() throws CatProtDaoException {
		
		Session session = null;
		List<T> result = new ArrayList<T>();
		try {
			session = getSession();
			result = session.createCriteria(classT).list();
		} catch (HibernateException e) {
			throw new CatProtDaoException("Something went wrong with all-getting transaction",e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	private void closeSession(Session session) {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}

	private Session getSession() {
		Session session;
		session = HibernateUtil.getSessionFactory().openSession();
		return session;
	}
}
