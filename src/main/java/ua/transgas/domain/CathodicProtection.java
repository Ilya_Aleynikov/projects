package ua.transgas.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "cathodic_protection")
public class CathodicProtection implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	long id;
	
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int passportNumber;
	
	@ManyToOne
	@Column(name="pipeline_id")
	Pipeline pipeline;
	
	@Column
	int location;

	@Column
	String protection;
	
	@Column
	String nearLocation;
	
	@Column
	Date startUp;
	
	@Column
	String city;
	
	@Column
	String projectCompany;
	
	@Column
	String constructCompany;
	
	@Column
	String landUser;
	
	public CathodicProtection() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(int passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Pipeline getPipeline() {
		return pipeline;
	}

	public void setPipeline(Pipeline pipeline) {
		this.pipeline = pipeline;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public String getProtection() {
		return protection;
	}

	public void setProtection(String protection) {
		this.protection = protection;
	}

	public String getNearLocation() {
		return nearLocation;
	}

	public void setNearLocation(String nearLocation) {
		this.nearLocation = nearLocation;
	}

	public Date getStartUp() {
		return startUp;
	}

	public void setStartUp(Date startUp) {
		this.startUp = startUp;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProjectCompany() {
		return projectCompany;
	}

	public void setProjectCompany(String projectCompany) {
		this.projectCompany = projectCompany;
	}

	public String getConstructCompany() {
		return constructCompany;
	}

	public void setConstructCompany(String constructCompany) {
		this.constructCompany = constructCompany;
	}

	public String getLandUser() {
		return landUser;
	}

	public void setLandUser(String landUser) {
		this.landUser = landUser;
	}
	
	

}
