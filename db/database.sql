CREATE DATABASE "Transgas"
  WITH OWNER = test
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Ukrainian_Ukraine.1251'
       LC_CTYPE = 'Ukrainian_Ukraine.1251'
       CONNECTION LIMIT = -1;
       
CREATE TABLE cathodic_protection
(
  id bigserial NOT NULL,
  passport_number smallserial NOT NULL,
  pipeline_id bigint NOT NULL,
  location smallint,
  protection text,
  near_location text,
  start_up date,
  city text,
  project_company text,
  construct_company text,
  land_user text,
  CONSTRAINT cathodic_protection_pkey PRIMARY KEY (id),
  CONSTRAINT cathodic_protection_pipeline_id_fkey FOREIGN KEY (pipeline_id)
      REFERENCES pipeline (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE TABLE pipeline
(
  id bigserial NOT NULL,
  name text NOT NULL,
  diameter integer,
  CONSTRAINT pipeline_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE cathode_converter
(
  id bigserial NOT NULL,
  name text NOT NULL,
  CONSTRAINT cathode_converter_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE anode_grounding
(
  id bigserial NOT NULL,
  name text NOT NULL,
  CONSTRAINT anode_grounding_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);